FROM ubuntu:20.04

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -yq nodejs npm curl

RUN curl -fsSL https://get.pulumi.com/ | bash

RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.18.0/bin/linux/amd64/kubectl && \
  chmod +x ./kubectl && \
  mv ./kubectl /usr/local/bin/kubectl

RUN curl -sL https://get.helm.sh/helm-v3.2.1-linux-amd64.tar.gz | tar xz && \
  chmod +x ./linux-amd64/helm && \
  mv ./linux-amd64/helm /usr/local/bin/helm && \
  rm -rf ./linux-amd64

RUN curl -sL https://github.com/digitalocean/doctl/releases/download/v1.43.0/doctl-1.43.0-linux-amd64.tar.gz | tar xz && \
  chmod +x ./doctl && \
  mv ./doctl /usr/local/bin/doctl
